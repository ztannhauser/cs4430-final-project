
// This file has been generated by the GUI designer. Do not modify.
namespace TestGTK
{
	public partial class Window
	{
		private global::Gtk.VBox vbox1;

		private global::Gtk.Button button1;

		private global::Gtk.ProgressBar progressbar1;

		private global::Gtk.Calendar calendar2;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget TestGTK.Window
			this.Name = "TestGTK.Window";
			this.Title = global::Mono.Unix.Catalog.GetString("Window");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Container child TestGTK.Window.Gtk.Container+ContainerChild
			this.vbox1 = new global::Gtk.VBox();
			this.vbox1.Name = "vbox1";
			this.vbox1.Spacing = 6;
			// Container child vbox1.Gtk.Box+BoxChild
			this.button1 = new global::Gtk.Button();
			this.button1.CanFocus = true;
			this.button1.Name = "button1";
			this.button1.UseUnderline = true;
			this.button1.Label = global::Mono.Unix.Catalog.GetString("GtkButton");
			this.vbox1.Add(this.button1);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox1[this.button1]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;
			// Container child vbox1.Gtk.Box+BoxChild
			this.progressbar1 = new global::Gtk.ProgressBar();
			this.progressbar1.Name = "progressbar1";
			this.vbox1.Add(this.progressbar1);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.vbox1[this.progressbar1]));
			w2.Position = 1;
			w2.Expand = false;
			w2.Fill = false;
			// Container child vbox1.Gtk.Box+BoxChild
			this.calendar2 = new global::Gtk.Calendar();
			this.calendar2.CanFocus = true;
			this.calendar2.Name = "calendar2";
			this.calendar2.DisplayOptions = ((global::Gtk.CalendarDisplayOptions)(35));
			this.vbox1.Add(this.calendar2);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.vbox1[this.calendar2]));
			w3.Position = 2;
			w3.Expand = false;
			w3.Fill = false;
			this.Add(this.vbox1);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 651;
			this.DefaultHeight = 346;
			this.Show();
		}
	}
}
