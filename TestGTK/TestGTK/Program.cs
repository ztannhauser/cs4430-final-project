﻿using System;
using Gtk;
using System.IO;

namespace TestGTK
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Application.Init();
            Window w = new Window();
            Application.Run();
        }
    }
}
