namespace NodeViewTutorial {
 
        [Gtk.TreeNode (ListOnly=true)]
        public class MyTreeNode : Gtk.TreeNode {
 
                string song_title;
 
                public MyTreeNode (string artist, string song_title)
                {
                        Artist = artist;
                        this.song_title = song_title;
                }
 
                [Gtk.TreeNodeValue (Column=0)]
                public string Artist;
 
                [Gtk.TreeNodeValue (Column=1)]
                public string SongTitle {get { return song_title; } }
        }
 
        public class NodeViewExample : Gtk.Window {
 
                Gtk.Label label;
 
                public NodeViewExample () : base ("NodeView")
                {
                        SetSizeRequest (300,225);
 
                        Gtk.Box box = new Gtk.VBox (false, 0);
                        Add (box);
 
                        label = new Gtk.Label ("Current Selection: ");
                        box.PackStart (label, false, false, 5);
 
                        store = new Gtk.NodeStore (typeof (MyTreeNode));
                        store.AddNode (new MyTreeNode ("The Beatles", "Yesterday"));
                        store.AddNode (new MyTreeNode ("oPeter Gabriel", "In Your Eyes"));
                        store.AddNode (new MyTreeNode ("Rush", "Fly By Night"));
                                
                        Gtk.NodeView view = new Gtk.NodeView (store);
                        box.PackStart (view, true, true, 5);
 
                        // Create a column with title Artist and bind its renderer to model column 0
                        view.AppendColumn ("Artist", new Gtk.CellRendererText (), "text", 0);
 
                        // Create a column with title 'Song Title' and bind its renderer to model column 1
                        view.AppendColumn ("Song Title", new Gtk.CellRendererText (), "text", 1);
//                        view.NodeSelection.Changed += new System.EventHandler (OnSelectionChanged);
                        box.ShowAll ();
                }
//                void OnSelectionChanged (object o, System.EventArgs args)
//                {
//                        Gtk.NodeSelection selection = (Gtk.NodeSelection) o;
//                        MyTreeNode node = (MyTreeNode) selection.SelectedNode;
//                        label.Text = "Current Selection: \"" + node.SongTitle + "\" by " + node.Artist;
//                }
 
                protected override bool OnDeleteEvent (Gdk.Event ev)
                {
                        Gtk.Application.Quit ();
                        return true;
                }
 
                Gtk.NodeStore store;
 
                public static void Main ()
                {
                        Gtk.Application.Init ();
                        NodeViewExample win = new NodeViewExample ();
                        win.Show ();
                        Gtk.Application.Run ();
                }
 
        }
}
