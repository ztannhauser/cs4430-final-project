﻿using System;
namespace FinalProject4430
{
    public partial class Window : Gtk.Window
    {
        public Window() :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();
            button2.Label = "Add Event";
            button2.Clicked += HandleEventHandler;
            button3.Label = "Remove Event";
            button4.Label = "Apply Filters";
        }

        AddEventDialog aed = new AddEventDialog();
        void HandleEventHandler(object sender, EventArgs e)
        {
            Console.Out.WriteLine("Click!");
            aed.Visible = true;
        }

        void Aed_EnterNotifyEvent(object o, Gtk.EnterNotifyEventArgs args)
        {
            aed.Visible = false;
        }

    }
}
