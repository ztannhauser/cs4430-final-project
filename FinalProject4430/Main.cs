﻿using System;
using Gtk;

namespace FinalProject4430
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            DB db = new DB();
            Application.Init();
            RootDialog gui = new RootDialog(db);
            Application.Run();
            db.commit();
            db.close();
        }
    }
}
