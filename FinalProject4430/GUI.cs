﻿using System;
using Gtk;
using System.Data;

namespace FinalProject4430
{
    public class GUI : Window
    {
        Button button1;
        Button button2;
        DB db;
        public GUI(DB db)
        {
            this.db = db;
            button1 = new Button();
            button2 = new Button();
            // Set the text of button1 to "OK".
            button1.Text = "OK";
            // Set the position of the button on the form.
            button1.Location = new Point(10, 10);
            // Set the text of button2 to "Cancel".
            button2.Text = "Cancel";
            // Set the position of the button based on the location of button1.
            button2.Location = new Point(button1.Left,
                button1.Height + button1.Top + 10);
            // Set the caption bar text of the form.   
            this.Text = "My Dialog Box";
            // Display a help button on the form.

            GridView myGridView = new GridView();
            myGridView.AllowsColumnReorder = true;
            myGridView.ColumnHeaderToolTip = "Employee Information";

            GridViewColumn gvc1 = new GridViewColumn();
            gvc1.DisplayMemberBinding = new Binding("FirstName");
            gvc1.Header = "FirstName";
            gvc1.Width = 100;
            myGridView.Columns.Add(gvc1);
            GridViewColumn gvc2 = new GridViewColumn();
            gvc2.DisplayMemberBinding = new Binding("LastName");
            gvc2.Header = "Last Name";
            gvc2.Width = 100;
            myGridView.Columns.Add(gvc2);
            GridViewColumn gvc3 = new GridViewColumn();
            gvc3.DisplayMemberBinding = new Binding("EmployeeNumber");
            gvc3.Header = "Employee No.";
            gvc3.Width = 100;
            myGridView.Columns.Add(gvc3);

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            // Set the accept button of the form to button1.
            this.AcceptButton = button1;
            // Set the cancel button of the form to button2.
            this.CancelButton = button2;
            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            // Add button1 to the form.
            this.Controls.Add(button1);
            // Add button2 to the form.
            this.Controls.Add(button2);

        }
    }
}
