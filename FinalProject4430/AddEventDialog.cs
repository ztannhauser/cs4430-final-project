﻿using System;
namespace FinalProject4430
{
    public partial class AddEventDialog : Gtk.Dialog
    {
        RootDialog parent;
        MyTreeNode editing;
        public AddEventDialog(RootDialog parent, MyTreeNode selectednode)
        {
            this.Build();
            this.parent = parent;
            this.spinbutton3.Digits = 0;
            this.spinbutton3.Digits = 0;
            this.spinbutton3.SetRange(1, 12);
            this.spinbutton3.Text = "1";
            this.spinbutton5.Text = "0";
            this.spinbutton5.SetRange(0, 59);
            this.spinbutton5.SetIncrements(15, 30);
            this.combobox5.InsertText(0,"None");
            this.combobox5.InsertText(1, "Daily");
            this.combobox5.InsertText(2, "Weekly");
            this.combobox5.InsertText(3, "Bi-Weekly");
            this.combobox5.InsertText(4, "Monthly");
            this.combobox5.InsertText(5, "Yearly");
            this.combobox5.Active = 0;
            this.buttonOk.Clicked += ButtonOk_Clicked;
            this.buttonCancel.Clicked += ButtonCancel_Clicked;
            foreach(string s in parent.colors)
            {
                combobox4.AppendText(s);
            }
            this.radiobutton2.Label = "AM";
            this.radiobutton4.Label = "PM";
            this.label13.Text = "Color:";
            this.label9.Text = "Hour:";
            this.label11.Text = "Minute:";
            this.label7.Text = "Repeat:";
            this.editing = selectednode;
            if (selectednode == null)
            {
                combobox4.Active = 0;
                this.Title = "Create New Event";
            }
            else
            {
                this.Title = "Editing Event \"" + selectednode.title + "\"";
                this.entry4.Text = selectednode.title;
                this.textview1.Buffer.Text = selectednode.description;
                this.calendar3.Date = selectednode.date;
                this.combobox5.Active = selectednode.frequency;
                for(int i = 0, n = parent.colors.Length; i < n; i++)
                {
                    if(parent.colors[i] == selectednode.Color)
                    {
                        combobox4.Active = i;
                        break;
                    }
                }
            }
        }

        void ButtonOk_Clicked(object sender, EventArgs e)
        {
            DateTime a =
                    this.calendar3.Date
                        .AddHours(spinbutton3.ValueAsInt + (this.radiobutton4.Active ? 12 : 0))
                        .AddMinutes(spinbutton5.ValueAsInt);
            if (this.editing == null)
            {
                parent.db.add_event
                (
                    a,
                    this.entry4.Text,
                    this.textview1.Buffer.Text,
                    this.combobox4.ActiveText,
                    this.combobox5.ActiveText
                );
            }
            else
            {
                parent.db.edit_event
                (
                    editing.eid,
                    a,
                    this.entry4.Text,
                    this.textview1.Buffer.Text,
                    this.combobox4.ActiveText,
                    this.combobox5.ActiveText
                );
                parent.GetNodeView().Sensitive = true;
            }
            parent.rebuild_nodeview();
            this.Hide();
        }

        void ButtonCancel_Clicked(object sender, EventArgs e)
        {
            if (this.editing != null)
            {
                parent.GetNodeView().Sensitive = true;
            }
            this.Hide();
        }

    }
}
