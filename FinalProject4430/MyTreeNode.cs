﻿using System;
using System.Drawing;
namespace FinalProject4430
{
    [Gtk.TreeNode(ListOnly = true)]
    public class MyTreeNode : Gtk.TreeNode
    {
        public MyTreeNode(uint eid, string title, string description, DateTime date, string color, bool isRepeated, string frequency)
        {
            this.eid = eid;
            this.title = title;
            this.date = date;
            this.description = description;
            this.date_string = date.ToLongDateString() + " " + date.ToLongTimeString();
            Color = color;
            this.isRepeated = isRepeated;
            this.frequency = Get_frequency(frequency);

        }
        public uint eid;
        public DateTime date;
        public string description;
        public bool isRepeated;
        public int frequency;

        [Gtk.TreeNodeValue(Column = 0)]
        public string title;

        [Gtk.TreeNodeValue(Column = 1)]
        public string date_string;

        [Gtk.TreeNodeValue(Column = 2)]
        public string Color;

        public int Get_frequency(string frequency)
        {
            int i = 0;

           switch(frequency)
            {
                case "None":
                    return i;
                    

                case "Daily":

                    return i + 1;

                case "Weekly":

                    return i + 2;
                    

                case "Bi-Weekly":
                    
                    return i + 3;
                    

                case "Monthly":

                    return i + 4;
                   
                case "Yearly":
                    
                    return i + 5;


            }
            return i;


        }

      

    }
}
