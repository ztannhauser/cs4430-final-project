﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace FinalProject4430
{
    public class DBConnector
    {
        String test;
        public DBConnector()
        {
		    string connectionString =
		            "Server=localhost;" +
		            "Database=northwind;" +
		            "User ID=cs4430;" +
		            "Password=cs4430;" +
		            "Pooling=false";
            IDbConnection dbcon;
            dbcon = new MySqlConnection(connectionString);
            dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();

            string sql =
                "SELECT FirstName, LastName " +
                "FROM employees";
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                string firstName = (string)reader["firstname"];
                string lastName = (string)reader["lastname"];
                test += (" Name: " + firstName +
                " " + lastName);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            dbcon.Close();
            dbcon = null;
        }

        public string Test()
        {
            return test;

        }
    }
}
