﻿using System;
using Gtk;
using System.Reflection;
using System.Collections.Generic;


namespace FinalProject4430
{
    public partial class RootDialog : Gtk.Dialog
    {
        List<DateTime> dateList = new List<DateTime>();
        Gtk.NodeStore store;
        public DB db;
        FilterDialog fd;
        MyTreeNode selectednode;
        public string[] colors = {
            "Black",
            "Gray",
            "Red",
            "PaleVioletRed",
            "IndianRed",
            "Orange",
            "Yellow",
            "DeepSkyBlue4",
            "LightGoldenrod",
            "Green",
            "Blue",
            "DarkTurquoise",
            "Purple",
            "Magenta",
        };

        public RootDialog(DB db)
        {
            this.Build();
            this.db = db;
            fd = new FilterDialog(this);
            button17.Label = "Add an Event";
            button17.Clicked += Button17_Clicked;
            button18.Label = "Remove an Event";
            button18.Clicked += Button18_Clicked;
            button19.Label = "Filter Settings";
            button19.Clicked += Button19_Clicked;
            button20.Label = "Event Info";
            button20.Sensitive = false;
            rebuild_nodeview();
            nodeview1.NodeSelection.Mode = SelectionMode.Single;
            nodeview1.NodeSelection.Changed += new System.EventHandler(NodeSelection_Changed);
            nodeview1.AppendColumn("Title", new Gtk.CellRendererText(), "text", 0, "foreground", 2);
            nodeview1.AppendColumn("Date", new Gtk.CellRendererText(), "text", 1, "foreground", 2);
            this.buttonOk.Clicked += ButtonOk_Clicked;
            void ButtonOk_Clicked(object sender, EventArgs e)
            {
                this.Hide();
                Application.Quit();
            }
            this.button20.Clicked += Button20_Clicked;
            this.Resizable = false;
        }


        void NodeSelection_Changed(object sender, EventArgs e)
        {
            Gtk.NodeSelection selection = (Gtk.NodeSelection)sender;
            if (selection.SelectedNode != null)
            {
                selectednode = (MyTreeNode)selection.SelectedNode;
                button20.Sensitive = true;
            }
        }

        public void rebuild_nodeview()
        {
            store = new Gtk.NodeStore(typeof(MyTreeNode));
            foreach (MyTreeNode node in
                db.get_list_of_events(
                    fd.get_filter_range_start(),
                    fd.get_filter_range_end(),
                    fd.get_filter_flag_color()
                    ))

            {
                if (node.date <= fd.get_filter_range_end())
                {



                    if (node.frequency == 0)
                    {

                        store.AddNode(node);

                    }

                    if (node.frequency == 1)
                    {


                        get_daily_repeat(node);

                        foreach (DateTime i in dateList)
                        {
                            if (i >= fd.get_filter_range_start())
                            {
                                MyTreeNode newNode = new MyTreeNode(node.eid, node.title, node.description, i, node.Color, node.isRepeated, "Daily");



                                store.AddNode(newNode);


                            }

                        }



                    }
                    if (node.frequency == 2)
                    {

                        get_weekly_repeat(node);

                        foreach (DateTime i in dateList)
                        {
                            if (i >= fd.get_filter_range_start())
                            {
                                MyTreeNode newNode = new MyTreeNode(node.eid, node.title, node.description, i, node.Color, node.isRepeated, "Weekly");


                                store.AddNode(newNode);

                            }




                        }
                    }

                    if (node.frequency == 3)
                    {

                        get_biweekly_repeat(node);

                        foreach (DateTime i in dateList)
                        {

                            if (i >= fd.get_filter_range_start())
                            {
                                MyTreeNode newNode = new MyTreeNode(node.eid, node.title, node.description, i, node.Color, node.isRepeated, "Bi-Weekly");



                                store.AddNode(newNode);

                            }


                        }
                    }

                    if (node.frequency == 4)
                    {

                        get_monthly_repeat(node);

                        foreach (DateTime i in dateList)
                        {

                            if (i >= fd.get_filter_range_start())
                            {
                                MyTreeNode newNode = new MyTreeNode(node.eid, node.title, node.description, i, node.Color, node.isRepeated, "Monthly");



                                store.AddNode(newNode);

                            }
                           

                        }
                    }

                    if (node.frequency == 5)
                    {

                        get_yearly_repeat(node);

                        foreach (DateTime i in dateList)
                        {
                            if(i >= fd.get_filter_range_start())
                            {
                                MyTreeNode newNode = new MyTreeNode(node.eid, node.title, node.description, i, node.Color, node.isRepeated, "Yearly");



                                store.AddNode(newNode);

                            }
                           

                        }
                    }
                }


            }

           

            nodeview1.NodeStore = store;
            typeof(NodeView).GetField("store",
                BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(nodeview1, store);
        }
        void Button17_Clicked(object sender, EventArgs e)
        {
            AddEventDialog aed = new AddEventDialog(this, null);
            aed.Show();
        }
        void Button18_Clicked(object sender, EventArgs e)
        {
            if (selectednode != null)
            {
                db.delete_event_with_id(selectednode.eid);
                store.RemoveNode(selectednode);
                button20.Sensitive = false;
                selectednode = null;

            }
        }
        void Button19_Clicked(object sender, EventArgs e)
        {
            fd.Show();
        }

        void Button20_Clicked(object sender, EventArgs e)
        {
            nodeview1.Sensitive = false;
            AddEventDialog aed = new AddEventDialog(this, selectednode);
            aed.Show();
        }

       public NodeView GetNodeView()
        {
            return nodeview1;
        }

        public void get_daily_repeat(MyTreeNode node)
        {
            dateList.Clear();

            DateTime start = fd.get_filter_range_start();
            DateTime end = fd.get_filter_range_end();


            dateList.Add(node.date);

            int i = 1;
            while (dateList[dateList.Count - 1] < end)
            {

                dateList.Add(node.date.AddDays(i));

               
                
                start = start.AddDays(1);
              
                i = i + 1;

            }

        }

        public void get_weekly_repeat(MyTreeNode node)
        {
            dateList.Clear();

            DateTime start = fd.get_filter_range_start();
            DateTime end = fd.get_filter_range_end();

            int i = 7;
            dateList.Add(node.date);
            while (start < end.AddDays(-1))
            {
                dateList.Add(node.date.AddDays(i));

                start = start.AddDays(7);

                i = i + 7;


            }
            if (end < dateList[dateList.Count - 1])
            {
                DateTime x = dateList[dateList.Count - 1];
                dateList.Remove(x);
            }





        }

        public void get_biweekly_repeat(MyTreeNode node)
        {
            dateList.Clear();

            DateTime start = fd.get_filter_range_start();
            DateTime end = fd.get_filter_range_end();

            int i = 14;
            dateList.Add(node.date);
            while (start < end.AddDays(-1))
            {
                dateList.Add(node.date.AddDays(i));

                start = start.AddDays(14);

                i = i + 14;


            }
            if (end < dateList[dateList.Count - 1])
            {
                DateTime x = dateList[dateList.Count - 1];
                dateList.Remove(x);
            }

        }

        public void get_monthly_repeat(MyTreeNode node)
        {
            dateList.Clear();

            DateTime start = fd.get_filter_range_start();
            DateTime end = fd.get_filter_range_end();
            dateList.Add(node.date);

            int i = 1;
            while (start < end.AddDays(-1))
            {
                dateList.Add(node.date.AddMonths(i));

                start = start.AddMonths(1);

                i = i + 1;


            }
        }

        public void get_yearly_repeat(MyTreeNode node)
        {
            dateList.Clear();

            DateTime start = fd.get_filter_range_start();
            DateTime end = fd.get_filter_range_end();
            dateList.Add(node.date);




            int i = 1;
            while (start < end.AddDays(-1))
            {
                dateList.Add(node.date.AddYears(i));

                start = start.AddYears(1);

                i = i + 1;

            }
            if(end < dateList[dateList.Count-1])
            {
                DateTime x = dateList[dateList.Count-1];
                dateList.Remove(x);
            }
        }

    }
}
