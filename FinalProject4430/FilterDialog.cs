﻿using System;
namespace FinalProject4430
{
    public partial class FilterDialog : Gtk.Dialog
    {
        RootDialog parent;
        MyTreeNode mytree;
        public FilterDialog(RootDialog parent)
        {
            this.Build();
            this.parent = parent;
            label4.Text = "Start Date:";
            label3.Text = "End Date:";
            label8.Text = "Only See Events of Color: ";
            calendar1.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            calendar2.Date = DateTime.Now.AddMonths(1);
            combobox2.AppendText("Any/All");
            foreach (string s in parent.colors)
            {
                combobox2.AppendText(s);
            }
            combobox2.Active = 0;
            this.buttonOk.Clicked += ButtonOk_Clicked;
            this.buttonCancel.Clicked += ButtonCancel_Clicked;
            this.Hide();
        }

        void ButtonOk_Clicked(object sender, EventArgs e)
        {
            this.Hide();
            parent.rebuild_nodeview();
        }

        void ButtonCancel_Clicked(object sender, EventArgs e)
        {
            this.Hide();
        }
       public  DateTime get_filter_range_start()
        {
            return this.calendar1.Date;
        }
       public  DateTime get_filter_range_end()
        {
            return this.calendar2.Date;
        }
        public string get_filter_flag_color()
        {
            return this.combobox2.ActiveText;
        }
        public DateTime get_repeating_dates()
        {
            DateTime i = DateTime.Now;

            DateTime start = this.calendar1.Date;
            DateTime end = this.calendar2.Date;

           

            

            return i;
        }

    }
}
