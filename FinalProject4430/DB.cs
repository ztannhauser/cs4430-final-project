﻿using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace FinalProject4430
{
    public class DB
    {
        // ## Tables ##
        // 
        // Event (eid, start_date, title, description, color, isRepeated, frequency);
        //

        IDbConnection dbcon;
        public DB()
        {
            string connectionString =
                    "Server=localhost;" +
                    "Database=calendar;" +
                    "User ID=cs4430;" +
                    "Password=cs4430;" +
                    "Pooling=false";
            dbcon = new MySqlConnection(connectionString);
            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS calendar " +
                "(" +
                    "eid INT UNSIGNED AUTO_INCREMENT PRIMARY KEY" + ", " +
                    "start_date DATETIME" + ", " +
                    "title VARCHAR(50)" + ", " +
                    "description VARCHAR(400)" + ", " +
                    "color VARCHAR(20)" + ", " +
                    "isRepeated bool" + ", " +
                    "fequency VARCHAR(10)" +

                ");";
            Console.Out.WriteLine(dbcmd.CommandText);
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();

        }
        string timeformat = "yyyy-MM-dd HH:mm:ss";
        public void add_event(DateTime day, string title, string description, string color, string frequency)
        {
            int isRepeated = 0;
            if (frequency != "None")
            {
                isRepeated = 1;

            }


            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText =
            "INSERT INTO calendar (start_date, title, description, color, isRepeated, frequency) VALUES ('" +
                day.ToString(timeformat) + "', " + 
                "'" + title + "', " + 
                "'" + description + "', " +
                "'" + color + "'," +
                "'" + isRepeated + "'," +
                "'" + frequency + 
                "');";
            Console.Out.WriteLine(dbcmd.CommandText);
            dbcmd.ExecuteNonQuery();

           
        }
        public void edit_event(uint eid, DateTime day, string title, string description, string color, string frequency)
        {
            int isRepeated = 0;
            if (frequency != "None")
            {
                isRepeated = 1;
            }

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText =
            "UPDATE calendar SET " + 
                "start_date = '" + day.ToString(timeformat) + "', " +
                "title = '" + title + "', " +
                "description = '" + description + "', " + 
                "color = '" + color + "', " + 
                "isRepeated = '" + isRepeated + "'," +
                "frequency = '" + frequency + "' " + 
                "WHERE eid = " + eid + ";";
            Console.Out.WriteLine(dbcmd.CommandText);
            dbcmd.ExecuteNonQuery();
        }
        public void delete_event_with_id(uint eid)
        {
            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText =
            "DELETE FROM calendar WHERE eid = " + eid + ";";
            dbcmd.ExecuteNonQuery();
        }
        public List<MyTreeNode> get_list_of_events(DateTime start, DateTime end, string fcolor)
        {
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT eid, title, description, start_date, color, isRepeated, frequency FROM calendar WHERE isRepeated = 1 OR " +
                "start_date >= '" + start.ToString(timeformat) + "' " +
                "and " +
                "start_date <= '" + end.ToString(timeformat) + "'"
            ;
            if(fcolor != "Any/All")
            {
                sql += "and color = '" + fcolor + "'";
            }
            dbcmd.CommandText = sql;
            Console.Out.WriteLine(dbcmd.CommandText);
            IDataReader reader = dbcmd.ExecuteReader();
            List < MyTreeNode > ret = new List<MyTreeNode>();
            while (reader.Read())
            {
                uint eid = (uint)reader["eid"];
                string title = (string)reader["title"];
                string description = (string)reader["description"];
                DateTime start_date = (DateTime)reader["start_date"];
                string color = (string)reader["color"];
                bool isRepeated = (bool)reader["isRepeated"];
                string frequency = (string)reader["frequency"];
                ret.Add(new MyTreeNode(eid, title, description, start_date, color, isRepeated, frequency));
            }
            reader.Close();
            dbcmd.Dispose();
            return ret;
        }

        public void commit()
        {
            // MYSQL.commit();
        }
        public void close()
        {
            dbcon.Close();
        }
    }
}
